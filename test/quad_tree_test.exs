defmodule QuadTreeTest do
  use ExUnit.Case
  doctest QuadTree

  setup do
    qt = start_supervised!(QuadTree)
    IO.inspect qt
    %{qt: qt}
  end

  test "add points", %{qt: qt} do
    QuadTree.add(qt, %{x: 15, y: 20})
    QuadTree.add(qt, %{x: 20, y: 15})
    QuadTree.add(qt, %{x: 30, y: 10})
    QuadTree.add(qt, %{x: 40, y: 20})
    QuadTree.add(qt, %{x: 50, y: 30})
    QuadTree.add(qt, %{x: 51, y: 30})
    QuadTree.add(qt, %{x: 50, y: 31})
    QuadTree.add(qt, %{x: 49, y: 30})
    QuadTree.add(qt, %{x: 50, y: 29})
    QuadTree.add(qt, %{x: 55, y: -55})
    QuadTree.add(qt, %{x: 32, y: 32})
    # |> IO.inspect
  end

  test "aggregate quads and points", %{qt: qt} do
    QuadTree.add(qt, %{x: 15, y: 20})
    QuadTree.add(qt, %{x: 20, y: 15})
    QuadTree.add(qt, %{x: 30, y: 10})
    QuadTree.add(qt, %{x: 40, y: 20})
    QuadTree.add(qt, %{x: 50, y: 30})
    QuadTree.add(qt, %{x: 51, y: 30})
    QuadTree.add(qt, %{x: 50, y: 31})
    QuadTree.add(qt, %{x: 49, y: 30})
    QuadTree.add(qt, %{x: 50, y: 29})
    QuadTree.add(qt, %{x: 55, y: -55})
    QuadTree.add(qt, %{x: 32, y: 32})

    assert length(QuadTree.agg(qt).points) == 11

    QuadTree.add(qt, %{x: 33, y: 33})
    qt_agg = QuadTree.agg(qt)
    IO.inspect qt_agg
    assert length(QuadTree.agg(qt).points) == 12
  end

  test "split quad w/o points" do
    qd = %{c: %{x: 0, y: 0}, r: 32, pts: []}
    split = QuadTree.split_quad(qd)
    assert split.ne.r == 16
    assert split.se.r == 16
    assert split.sw.r == 16
    assert split.nw.r == 16
  end

  test "split quad w/ points" do
    qd = %{
      c: %{x: 0, y: 0},
      r: 32,
      pts: [
        %{x: 1, y: 1},
        %{x: 2, y: 2}
      ]
    }
    split = QuadTree.split_quad(qd)
    assert length(split.ne.pts) == 0
    assert length(split.se.pts) == 0
    assert length(split.sw.pts) == 0
    assert length(split.nw.pts) == 2
  end

end
