defmodule ChaosGame do
  # def init_agent_matrix(agents_tup, cur, state_func, matrix) do
  #   {maxx,maxy} = agents_tup
  #   case cur do
  #     {maxx, maxy} -> {:done, matrix}
  #     {maxx, _} -> {:next, {0,maxy+1}, matrix}
  #     {_, _} -> {:fill, 



  # matrix = []
  # agents_tup = {5,5} # x,y
  # cur = {0,0} # x,y

  def f(len, list, init) do
    case len do
      0 -> list
      _ -> f(len-1, [init.(len, 0) | list], init)
    end
  end

  def main() do
    agent_list = ChaosGame.f(
      5, [], &Agent.start_link(
        fn -> %{x: &1, y: &2} end
      )
    )

    for agent_tup <- agent_list,
      {:ok, agent} = agent_tup
    do
        Agent.get(agent, fn pnt -> pnt end)
    end
  end
end

# Agents need to occupy a space with a defined neighbor topology
# for neighbor discovery and interaction

# quadtrees could be a good choice for sparse sims, regular grid
# for dense sims

# Outline:
#  Agents pass messages to the quadtree/grid on their updates
#   (may be issues with atomicity?)
#  Agents also pull from their "local" grid for neighbor info

defmodule QuadTree do
  use GenServer

  QT_CAP = 4

  @impl true
  def init(:ok) do
    # qt = []
    qt = %{
      ne: %{c: %{x: -64, y: 64}, r: 64, pts: []},
      se: %{c: %{x: -64, y: -64}, r: 64, pts: []},
      sw: %{c: %{x: 64, y: -64}, r: 64, pts: []},
      nw: %{c: %{x: 64, y: 64}, r: 64, pts: []}
    }
    {:ok, qt}
  end

  def which_quad?(qt, pt) do
    for quad <- qt,
      quad_dat = elem(quad,1),
      (
        pt.x > quad_dat.c.x - quad_dat.r
        and pt.x < quad_dat.c.x + quad_dat.r
        and pt.y > quad_dat.c.y - quad_dat.r
        and pt.y < quad_dat.c.y + quad_dat.r
      ) do
        quad
    end
    |> hd
  end

  def fill(qt, pts) do
    case pts do
      [pt | t] ->
        add(qt, pt)
        |> fill(t)
      [] -> qt
    end
  end

  def split_quad(quad_dat) do
    new_r = Kernel.div(quad_dat.r,2)
    qt = %{
      ne: %{c: %{x: quad_dat.x-new_r, y: quad_dat.y+new_r}, r: new_r, pts: []},
      se: %{c: %{x: quad_dat.x-new_r, y: quad_dat.y-new_r}, r: new_r, pts: []},
      sw: %{c: %{x: quad_dat.x+new_r, y: quad_dat.y-new_r}, r: new_r, pts: []},
      nw: %{c: %{x: quad_dat.x+new_r, y: quad_dat.y+new_r}, r: new_r, pts: []}
    }
    fill(qt, quad_dat.pts)

  def add(qt, pt) do
    new_quad = case quad = which_quad?(qt, pt), quad_dat = elem(quad, 1) do
      List.size(quad_dat.pts) >= QT_CAP ->
        %{quad | :pts => split_quad(%{quad_dat | :pts => [pt | quad_dat.pts]})}
        # eval to a quad
      [_|_] = quad_dat.pts ->
        # list is not yet full, prepend point
        %{quad | :pts => [pt | quad_dat.pts]}
      %{_} = quad_dat.pts ->
        # not at base quad, recurse
        add(quad.pts, pt)
    %{qt | elem(quad, 0) => new_quad}

  @impl true
  def handle_cast({:add, point}, from, qt) do
    #impl
  end

  @impl true
  def handle_cast({:move, point}, from, qt) do
    #impl
  end

  @impl true
  def handle_call({:neighbors, point}, from, qt) do
    #impl
  end
end
