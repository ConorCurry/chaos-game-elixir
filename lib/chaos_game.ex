defmodule ChaosGame do
  # def init_agent_matrix(agents_tup, cur, state_func, matrix) do
  #   {maxx,maxy} = agents_tup
  #   case cur do
  #     {maxx, maxy} -> {:done, matrix}
  #     {maxx, _} -> {:next, {0,maxy+1}, matrix}
  #     {_, _} -> {:fill, 



  # matrix = []
  # agents_tup = {5,5} # x,y
  # cur = {0,0} # x,y

  def f(len, list, init) do
    case len do
      0 -> list
      _ -> f(len-1, [init.(len, 0) | list], init)
    end
  end

  def main() do
    agent_list = ChaosGame.f(
      5, [], &Agent.start_link(
        fn -> %{x: &1, y: &2} end
      )
    )

    for agent_tup <- agent_list,
      {:ok, agent} = agent_tup
    do
        Agent.get(agent, fn pnt -> pnt end)
    end
  end
end

# Agents need to occupy a space with a defined neighbor topology
# for neighbor discovery and interaction

# quadtrees could be a good choice for sparse sims, regular grid
# for dense sims

# Outline:
#  Agents pass messages to the quadtree/grid on their updates
#   (may be issues with atomicity?)
#  Agents also pull from their "local" grid for neighbor info
