defmodule QuadTree do
  use GenServer
  require Logger

  # QT_CAP = 4
  defmodule Point do
    defstruct x: 0, y: 0
  end

  defmodule Quad do 
    defstruct c: %Point{}, r: 1, pts: []
  end

  defmodule QT do
    defstruct ne: %Quad{}, se: %Quad{}, sw: %Quad{}, nw: %Quad{}
  end

  @impl true
  def init(:ok) do
    qt = %QT{
      ne: %Quad{c: %Point{x: -64, y: 64}, r: 64, pts: []},
      se: %Quad{c: %Point{x: -64, y: -64}, r: 64, pts: []},
      sw: %Quad{c: %Point{x: 64, y: -64}, r: 64, pts: []},
      nw: %Quad{c: %Point{x: 64, y: 64}, r: 64, pts: []}
    }
    {:ok, qt}
  end

  def which_quad?(qt, pt) do
    for quad <- Map.from_struct(qt),
      {_dir, quad_dat} = quad,
      (
        pt.x >= quad_dat.c.x - quad_dat.r
        and pt.x < quad_dat.c.x + quad_dat.r
        and pt.y >= quad_dat.c.y - quad_dat.r
        and pt.y < quad_dat.c.y + quad_dat.r
      ) do
        quad
    end
    |> hd
  end

  def fill(qt, pts) do
    case pts do
      [pt | t] ->
        add_h(qt, pt)
        |> fill(t)
      [] -> qt
    end
  end

  def split_quad(quad_dat) do
    new_r = Kernel.div(quad_dat.r,2)
    qt = %QT{
      ne: %Quad{c: %Point{x: quad_dat.c.x-new_r, y: quad_dat.c.y+new_r}, r: new_r, pts: []},
      se: %Quad{c: %Point{x: quad_dat.c.x-new_r, y: quad_dat.c.y-new_r}, r: new_r, pts: []},
      sw: %Quad{c: %Point{x: quad_dat.c.x+new_r, y: quad_dat.c.y-new_r}, r: new_r, pts: []},
      nw: %Quad{c: %Point{x: quad_dat.c.x+new_r, y: quad_dat.c.y+new_r}, r: new_r, pts: []}
    }
    fill(qt, quad_dat.pts)
  end

  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  defp add_h(qt, pt) do
    {dir,quad} = which_quad?(qt, pt)
    new_quad = case quad.pts do
      pts when length(pts) >= 4 -> # QT_CAP ->
        # Logger.debug("split")
        %Quad{quad | pts: split_quad(%Quad{quad | pts: [pt | pts]})}
        # eval to a quad
      pts when is_list(pts) ->
        # list is not yet full, prepend point
        # Logger.debug("prepend")
        %Quad{quad | pts: [pt | pts]}
      %{} ->
        # not at base quad, recurse
        # Logger.debug("recurse")
        %Quad{quad | pts: add_h(quad.pts, pt)}
    end
    case new_quad do
      %{c: _} -> :noop
      # _ -> Logger.debug(new_quad)
    end
    case dir do
      :ne -> %QT{qt | ne: new_quad}
      :se -> %QT{qt | se: new_quad}
      :sw -> %QT{qt | sw: new_quad}
      :nw -> %QT{qt | nw: new_quad}
    end
  end

  def add(server, pt) do
    GenServer.cast(server, {:add, pt})
  end

  def agg(server) do
    GenServer.call(server, {:agg})
  end

  defp qd_agg(%Quad{pts: %QT{} = qd}, acc) do
    qd
    |> Map.from_struct
    |> Map.values
    |> Enum.map(fn %Quad{} = quad -> qd_agg(quad, acc) end)
    |> List.foldl(%{quads: [], points: []}, fn x, acc -> %{x | points: x.points ++ acc.points, quads: x.quads ++ acc.quads} end)
  end

  defp qd_agg(%Quad{pts: pts} = qd, acc) when is_list(pts) do
    %{acc |
      quads: [%{c: qd.c, r: qd.r} | acc.quads],
      points: pts ++ acc.points
    }
  end

  @impl true
  def handle_call({:agg}, _from, %QT{} = qt) do
    res = qt
    |> Map.from_struct
    |> Map.values
    |> Enum.map(fn %Quad{} = quad -> qd_agg(quad, %{quads: [], points: []}) end)
    |> List.foldl(%{quads: [], points: []}, fn x, acc -> %{x | points: x.points ++ acc.points, quads: x.quads ++ acc.quads} end)
    {:reply, res, qt}
  end

  @impl true
  def handle_cast({:add, pt}, qt) do
    {dir,quad} = which_quad?(qt, pt)
    new_quad = case quad.pts do
      pts when length(pts) >= 4 -> # QT_CAP ->
        # Logger.debug("split")
        %Quad{quad | pts: split_quad(%Quad{quad | pts: [pt | pts]})}
      pts when is_list(pts) ->
        # list is not yet full, prepend point
        # Logger.debug("prepend")
        %Quad{quad | pts: [pt | pts]}
      %{} ->
        # not at base quad, recurse
        # Logger.debug("recurse")
        %Quad{quad | pts: add_h(quad.pts, pt)}
    end

    new_state = case dir do
      :ne -> %QT{qt | ne: new_quad}
      :se -> %QT{qt | se: new_quad}
      :sw -> %QT{qt | sw: new_quad}
      :nw -> %QT{qt | nw: new_quad}
    end
    {:noreply, new_state}
  end

  #@impl true
  #def handle_cast({:del, point}, qt) do
  #  #impl
  #end

  #@impl true
  #def handle_call({:neighbors, point}, from, qt) do
  #  #impl
  #end
end
